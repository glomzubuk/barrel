﻿using System;
using BepInEx;
using BepInEx.Logging;
using LLBT;

namespace barrel
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency(LLBT.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInProcess("LLBlaze.exe")]
    public class barrel : BaseUnityPlugin
    {
        public static barrel Instance { get; private set; } = null;
        public static ManualLogSource Log { get; private set; } = null;

        public static BarrelTweak barrelTweak;

        void Awake()
        {
            Instance = this;
            Log = this.Logger;

            barrelTweak = new BarrelTweak();
            LLBTweaker.AddTweak(barrelTweak);
            Logger.LogDebug("Added barrel tweak");
        }
    }
}
