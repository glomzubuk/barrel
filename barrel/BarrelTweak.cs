using System;
using BepInEx.Configuration;
using GameplayEntities;
using HarmonyLib;
using LLBML;
using LLBML.Math;
using LLBML.Networking;
using LLBML.States;
using LLBML.Utils;
using LLBT.Tweaks;
using LLHandlers;
using Multiplayer;

namespace barrel
{
    public class BarrelTweak : HarmonyTweak
    {
        private int DEFAULT_MAX_COMMON_ITEMS = -1;
        internal readonly ConfigEntry<int> maxBarrel;
        internal readonly ConfigEntry<int> barrelSpawnInterval;

        private int gameStartTime = -1;

        public BarrelTweak() : base("barrel-tweak", "barrel")
        {
            DEFAULT_MAX_COMMON_ITEMS = ItemHandler.MAX_COMMON_ITEMS;

            barrelSpawnInterval = Config.Bind(this.ID, "barrelSpawnInterval", 5,
                new ConfigDescription(
                    "Time between barrel spawns in second.",
                    new AcceptableValueRange<int>(1, 120)
                )
            );
            maxBarrel = Config.Bind(this.ID, "maxBarrels", 1,
                new ConfigDescription(
                    "Maximum amount of barrels on screen.",
                    new AcceptableValueRange<int>(1, 20)
                )
            );
            this.AddConfig(barrelSpawnInterval);
            this.AddConfig(maxBarrel);

            this.AddPatchClass(typeof(barrel_Patches));
        }

        protected override void DoPatch()
        {
            base.DoPatch();
            ItemHandler.MAX_COMMON_ITEMS = maxBarrel.Value;
            ItemHandler.MAX_ITEMS = ItemHandler.MAX_COMMON_ITEMS + ItemHandler.MAX_CORPSE_ITEMS + ItemHandler.MAX_BOMB_ITEMS;
        }

        protected override void DoUnpatch()
        {
            base.DoUnpatch();
            ItemHandler.MAX_COMMON_ITEMS = DEFAULT_MAX_COMMON_ITEMS ;
            ItemHandler.MAX_ITEMS = ItemHandler.MAX_COMMON_ITEMS + ItemHandler.MAX_CORPSE_ITEMS + ItemHandler.MAX_BOMB_ITEMS;
        }

        public override void FixedUpdate()
        {
            if (GameStates.GetCurrent() == GameState.GAME_INTRO) gameStartTime = -1;
        }

        public void OnFrameUpdate()
        {
            if (!IsEnabled) return;

            GameState currentState = GameStates.GetCurrent();
            if (currentState == GameState.GAME || currentState == GameState.GAME_PAUSE)
            {
                BallEntity ball = BallApi.GetBall();
                if (gameStartTime == -1)
                {
                    if (ball?.ballData?.ballState != null && ball.ballData.ballState == BallState.SERVE)
                    {
                        gameStartTime = GameStatesGameUtils.currentFrame;
                    }
                }
                else
                {
                    int timeSinceStart = GameStatesGameUtils.currentFrame - gameStartTime;
                    int intervalInFrames = World.FPS * barrelSpawnInterval.Value;
                    if (timeSinceStart % intervalInFrames == 0)
                    {
                        // barrel.Log.LogDebug($"ControlledRandom state: {ControlledRandom.GetState()}");
                        // barrel.Log.LogDebug($"Spawning barrel, time:{GameStatesGameUtils.currentFrame}, interval: {intervalInFrames}, timeSinceStart: {timeSinceStart}");
                        Vector2f stageCenter = World.instance.GetStageCenter();
                        Vector2f stageMax = World.instance.stageMax;
                        Vector2f stageMin = World.instance.stageMin;
                        Floatf spawnPosX = ControlledRandom.GetF(1, stageMin.x, stageMax.x);

                        ItemHandler.instance.SpawnItem(ItemType.COMMON, new Vector2f(spawnPosX, stageMax.y));
                    }
                }
            }
        }
    }
    public static class barrel_Patches
    {

        [HarmonyPatch(typeof(World), nameof(World.FrameUpdate))]
        [HarmonyPrefix]
        public static bool FrameUpdate_Prefix()
        {
            if (NetworkApi.IsOnline && !Sync.isActive) return true;

            if (GameStatesGameUtils.currentFrame <= World.startHandlingFromFrame) return true;

            barrel.barrelTweak.OnFrameUpdate();
            return true;
        }
    }
}
