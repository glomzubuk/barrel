﻿using System.Reflection;
using barrel;

#region Assembly attributes
[assembly: AssemblyVersion(PluginInfos.PLUGIN_VERSION)]
[assembly: AssemblyTitle(PluginInfos.PLUGIN_NAME + " (" + PluginInfos.PLUGIN_ID + ")")]
[assembly: AssemblyProduct(PluginInfos.PLUGIN_NAME)]
#endregion

namespace barrel
{
    public static class PluginInfos
    {
        public const string PLUGIN_NAME = "barrel";
        public const string PLUGIN_ID = "fr.glomzubuk.plugins.llb.barrel";
        public const string PLUGIN_VERSION = "0.2.1";
    }
}
